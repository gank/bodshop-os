// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: 'product-6d4b5e'
})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  try {

    for(var i=0;i<event.sp.length;i++){
      const pro = await db.collection('shop_product').doc(event.sp[i]._id).get()
      console.log(pro.data.sale)
      await db.collection('shop_product').doc(event.sp[i]._id).update({
        // data 传入需要局部更新的数据
        data: {
          // 表示将 done 字段置为 true
          sale: pro.data.sale+parseInt(event.sp[i].num)
        }
      })


      const pro2 = await db.collection('index_diy').orderBy('createtime', 'desc').limit(1).get()
      console.log(pro2)
      if(pro2.data.length>0)
      for(var j=0;j<pro2.data[0].all.length;j++){
      
        for(var k=0;k<pro2.data[0].all[j].content.length;k++){
          
          if (pro2.data[0].all[j].content[k].type =='goods'){
            for (var l = 0; l < pro2.data[0].all[j].content[k].product_list.length;l++){
              if (pro2.data[0].all[j].content[k].product_list[l].id == event.sp[i]._id){
                let dd = pro2.data[0].all;
                dd[j].content[k].product_list[l].sale = dd[j].content[k].product_list[l].sale + parseInt(event.sp[i].num)
          
                await db.collection('index_diy').doc(pro2.data[0]._id).update({
                  // data 传入需要局部更新的数据
                  data: {
                    // 表示将 done 字段置为 true
                    all: dd                  }
                })
              }
            }
          }
        }
        
      }

      
    }

    return await db.collection('order_info').add({
      // data 字段表示需新增的 JSON 数据
      data: {
        diyId:event.diyId,
        sp: event.sp,
        openid: event.userInfo.openId,
        ordernumber:event.ordernumber,
        price: event.price,
        address: event.address,
        name: event.name,
        phone: event.phone,
        remark: event.remark,
        payment:event.payment,
        freeprice:event.freeprice,
        couponprice:event.couponprice,
        check:0,
        state: event.state,  // 0代付款 1 待发货 2 待收货 3退/换货  
        yf:event.yf,//运费
        tkyy:'',
        tkje: 0,
        tkwtms: '',
        createtime: event.createtime
      }
    })
  } catch (e) {
    console.error(e)
  }
}
async function getproid(id) {
   return await db.collection('shop_product').doc(id).get();
}