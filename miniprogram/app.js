//app.js
const util = require("./utils/util.js");
App({
  ext: {
    color: "#3D94FF"
  }, 
  globalData: {
    tmpCurrentPageName: '',
    diyId:'',
    diyEdit:'',
    diyProduct:'',
    diyAll:[],
    diyName:'',
    diyPageI:-1,
    isAdmin:0,
    branch:0
  },
  onLaunch: function () {
    
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        traceUser: true,
         env:'product-6d4b5e'
      })
    }
  }  
})
