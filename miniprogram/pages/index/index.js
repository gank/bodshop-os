//index.js
const app = getApp()
const db = wx.cloud.database()

Page({
  data: {

  },
  onLoad: function() {
    
    //初始化数据 diyEdit diy状态；0非编辑状态 1编辑状态
    app.globalData.diyEdit = 0;
    wx.cloud.callFunction({
      name: 'getOpenid',
      complete: res => {
        console.log("访问用户的openid:" + res.result.openid)
        db.collection('init_data').get().then(res2 => {
          if(res2.data.length>0){
            if (res2.data[0].adminOpenID == res.result.openid) {
              app.globalData.isAdmin = 1;
            }
            if(res2.data[0].branch=="saas"){
              wx.redirectTo({
                url: `/pages/manage/xcx-man/index`
              });
            }else{
              db.collection('index_diy').get().then(res => {
                if (res.data.length > 0) {
                  //value 2表示默认跳转官网小程序页面 1表示默认跳转商城小程序页面
                  app.globalData.diyId = res.data[0]._id;
                  if (res.data[0].product == "1") {
                    app.globalData.diyProduct = "1";
                    wx.switchTab({
                      url: `/pages/com/shop/index`
                    });
                  } else if (res.data[0].product == "2") {
                    app.globalData.diyProduct = "2";
                    wx.redirectTo({
                      url: `/pages/com/home/index`
                    });
                  }
                } else {
                  //首次跳转  默认页面
                  wx.redirectTo({
                    url: `/pages/com/home/index`
                  });
                }
              });
            }
          }
        })


      }
    })
    
  },
})