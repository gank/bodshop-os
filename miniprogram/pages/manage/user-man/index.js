const util = require("../../../utils/util.js");
Page({
    data : {
        cities : [],
        tmpcit : []
    },
    onChange(event){
        console.log(event.detail,'click right menu callback data')
    },
    onReady(){
            },
    onLoad:function(options){
      const db = wx.cloud.database()
      db.collection('user_list').get().then(res => {
         if(res.data.length>0){
           let t = [];
           for(var i=0;i<res.data.length;i++){
             let m = { "label": "常州Changzhou0519", "name": res.data[i].nick_name, "pinyin": util.chineseToPinYin(res.data[i].nick_name), "zip": "0519" }
             t.push(m)
           }
           this.setData({
             tmpcit : t
           })
           let storeCity = new Array(26);
           const words = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
           words.forEach((item, index) => {
             storeCity[index] = {
               key: item,
               list: []
             }
           })
           console.log(this.data.tmpcit)
           console.log(123)

           this.data.tmpcit.forEach((item) => {
             let firstName = item.pinyin.substring(0, 1);
             let index = words.indexOf(firstName);
             storeCity[index].list.push({
               name: item.name,
               key: firstName
             });
           })
           this.data.cities = storeCity;
           this.setData({
             cities: this.data.cities
           })


         }
      })
    }
});